import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  myForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      FirstName: '',
      LastName: '',
      email: '',
      password: ''
    })
    this.myForm.valueChanges.subscribe(console.log); //ovo ce nam pomoci da pratimo sta se desava na konzoli
  }

}
