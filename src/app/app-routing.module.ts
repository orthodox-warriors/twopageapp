import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserEditComponent } from './user-edit/user-edit.component';
// import {NotFoundComponent} from './not-found/not-found.component';

const routes: Routes = [
  { path: 'users', component: UserDetailsComponent },
  { path: 'users/new', component: UserEditComponent },
  { path: 'users/edit/:userId', component: UserEditComponent },
  { path: '', component: UserDetailsComponent }
  // ,
  // { path: '**', component: NotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
