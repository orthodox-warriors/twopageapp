import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  users:User[];

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.users = this.userService.getUsers();
  }
  DeleteUser(id: number): void {
    this.userService.removeUser(id);
    this.users = this.userService.getUsers();
  }
  EditUser(id: number): void {
    this.router.navigate(['/users/edit/' + id.toString()]);
  }

}
