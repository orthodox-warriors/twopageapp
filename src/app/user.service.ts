import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[] = [];

  constructor() {
    this.addUser(new User(this.currentMaxId() + 1, 'Nikola', 'Tesla', 'nikola.tesla@test.com', 'pass1'));
    this.addUser(new User(this.currentMaxId() + 1, 'Mihajlo', 'Pupin', 'mihajlo.pupin@test.com', 'pass2'));
    this.addUser(new User(this.currentMaxId() + 1, 'Mileva', 'Ajnstajn-Maric', 'mileva.maric@test.com', 'pass3'));
    this.addUser(new User(this.currentMaxId() + 1, 'Milutin', 'Milankovic', 'milutin.milankovic@test.com', 'pass4'));
    this.addUser(new User(this.currentMaxId() + 1, 'Jovan', 'Cvijic', 'jovan.cvijic@test.com', 'pass5'));
  }

  currentMaxId(): number {
    if (this.users.length < 1) { return 0; }
    if (this.users.length === 1) { return 1; }
    const maxId = this.users.map(user => user.id).reduce((a, b) => Math.max(a, b));
    return maxId;
  }
  addUser(user: User): void {
    this.users.push(user);
  }
  getUsers(): User[] {
    return this.users;
  }
  getUser(id: number): User {
    return this.users.find(item => item.id === id);
  }
  updateUser(user: User): void {
    this.users[user.id] = user;
  }
  removeUser(id: number): void {
    this.users = this.users.filter(item => item.id !== id);
  }
  createEmptyUser(): User {
    return new User(this.currentMaxId() + 1, '', '', '', '');
  }
}

  
  

    // getUsers(): any { 
    //   let users = User [    ];
    //   // { id: 1, firstName: 'Nikola', lastName: 'Tesla', email: 'nikola.tesla@gmail.com', password: 'pass1'},
    //   // { id: 1, firstName: 'Mihajlo', lastName: 'Pupin', email: 'mihajlo.pupin@gmail.com', password: 'pass2'}
       
    //   return (users);

    //   }
  // getData(users) 
  //   console.warn("data")
  // }

  // getUsers(): Observable<User[]> {
  //   return of(users);
  // }

