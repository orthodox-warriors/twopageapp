import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  user: User;
  selectedId: number;
  isNew: boolean;


  constructor( private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isNew = false;
    if (this.router.url.indexOf('new') !== -1) {
      this.user = this.userService.createEmptyUser();
      this.selectedId = this.user.id;
      this.isNew = true;
    } else {
      this.selectedId = Number(this.route.snapshot.paramMap.get('userId'));
      this.user = this.userService.getUser(this.selectedId);
    }
  }
  SaveUser(user: User): void {
    if (this.isNew) {
      this.userService.addUser(user);
    }

    this.router.navigate(['/users/']); //ovo vraca na pocetnu stranicu posle upisa
  }
  goBack() {
    this.router.navigate(['users/'])
  }
}
